-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2023-04-26 16:28:01
-- サーバのバージョン： 10.4.27-MariaDB
-- PHP のバージョン: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `pizza365_db`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `menu_combo`
--

CREATE TABLE `menu_combo` (
  `id` bigint(20) NOT NULL,
  `combo` char(1) DEFAULT NULL,
  `drink` int(11) DEFAULT NULL,
  `grilled` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `salad` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- テーブルのデータのダンプ `menu_combo`
--

INSERT INTO `menu_combo` (`id`, `combo`, `drink`, `grilled`, `price`, `salad`, `size`) VALUES
(1, 'S', 2, 2, 150000, '200gr', '20cm'),
(2, 'M', 4, 3, 200000, '250gr', '25cm'),
(3, 'L', 6, 5, 250000, '300gr', '30cm'),
(4, 'X', 8, 5, 500000, '550gr', '35cm');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `menu_combo`
--
ALTER TABLE `menu_combo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_qfnfomujs9a2b3i9jl6fut22h` (`combo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
