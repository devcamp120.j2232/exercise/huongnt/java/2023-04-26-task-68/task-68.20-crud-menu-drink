-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2023-04-26 17:45:06
-- サーバのバージョン： 10.4.27-MariaDB
-- PHP のバージョン: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `pizza365_db`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `ma_nuoc_uong` varchar(255) DEFAULT NULL,
  `gia_nuoc_uong` bigint(20) DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- テーブルのデータのダンプ `drinks`
--

INSERT INTO `drinks` (`id`, `created_at`, `ma_nuoc_uong`, `gia_nuoc_uong`, `ten_nuoc_uong`, `updated_at`) VALUES
(1, '2023-04-15 00:00:00', 'MT', 15000, 'Milk tea', '2023-04-15 00:00:00'),
(2, '2023-04-15 00:00:00', 'KT', 10000, 'Kumquat tea', '2023-04-15 00:00:00'),
(3, '2023-04-15 00:00:00', 'COKE', 15000, 'Cocacola', '2023-04-15 00:00:00'),
(4, '2023-04-15 00:00:00', 'PEP', 12000, 'Pepsi', '2023-04-15 00:00:00'),
(5, '2023-04-15 00:00:00', 'WTER', 10000, 'Water', '2023-04-15 00:00:00'),
(6, '2023-04-15 00:00:00', 'OT', 13000, 'Olong tea', '2023-04-15 00:00:00');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ioautl9bhwm9nmdgersk8mjx` (`ma_nuoc_uong`);

--
-- ダンプしたテーブルの AUTO_INCREMENT
--

--
-- テーブルの AUTO_INCREMENT `drinks`
--
ALTER TABLE `drinks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
