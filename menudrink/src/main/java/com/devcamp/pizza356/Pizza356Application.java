package com.devcamp.pizza356;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza356Application {

	public static void main(String[] args) {
		SpringApplication.run(Pizza356Application.class, args);
	}

}
