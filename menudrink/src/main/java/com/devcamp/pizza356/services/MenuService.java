package com.devcamp.pizza356.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza356.models.CMenu;
import com.devcamp.pizza356.repository.IMenuRepository;

@Service
public class MenuService {
    @Autowired
    IMenuRepository pMenuRepository;

    // get all menu
    public ArrayList<CMenu> getAllMenu() {
        ArrayList<CMenu> menuList = new ArrayList<>();
        pMenuRepository.findAll().forEach(menuList::add);
        return menuList;
    }

    // create
    public CMenu createMenu(CMenu cMenu) {
        try {
            CMenu savedMenu = pMenuRepository.save(cMenu);
            return savedMenu;
        } catch (Exception e) {
            return null;
        }
    }

    // update
    public CMenu updateMenu(long id, CMenu cMenu) {
        if (pMenuRepository.findById(id).isPresent()) {
            CMenu newUser = pMenuRepository.findById(id).get();
            newUser.setCombo(cMenu.getCombo());
            newUser.setSize(cMenu.getSize());
            newUser.setGrilled(cMenu.getGrilled());
            newUser.setSalad(cMenu.getSalad());
            newUser.setDrink(cMenu.getDrink());
            newUser.setPrice(cMenu.getPrice());
            CMenu savedMenu = pMenuRepository.save(newUser);
            return savedMenu;
        } else {
            return null;
        }
    }

    //delete by id
    
}
