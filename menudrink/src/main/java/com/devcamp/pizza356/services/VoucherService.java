package com.devcamp.pizza356.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza356.models.CVoucher;
import com.devcamp.pizza356.repository.IVoucherRepository;

@Service
public class VoucherService {
    @Autowired
    IVoucherRepository pVoucherRepository;  
    //get all voucher
    public ArrayList<CVoucher> getAllVoucher() {
        ArrayList<CVoucher> voucherList = new ArrayList<>();
        pVoucherRepository.findAll().forEach(voucherList::add);
        return voucherList;
    }
}
