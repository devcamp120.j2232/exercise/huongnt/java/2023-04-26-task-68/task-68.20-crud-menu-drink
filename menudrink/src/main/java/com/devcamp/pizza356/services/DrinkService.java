package com.devcamp.pizza356.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza356.models.CDrink;
import com.devcamp.pizza356.repository.IDrinkRepository;

@Service
public class DrinkService {
    @Autowired
    IDrinkRepository pDrinkRepository;

    //get all
    public ArrayList<CDrink> getAllDrinks() {
        ArrayList<CDrink> drinkList = new ArrayList<>();
        pDrinkRepository.findAll().forEach(drinkList::add);
        return drinkList;
    }
    
    // create
    public CDrink createMenu(CDrink cDrink) {
        try {
            CDrink savedDrink = pDrinkRepository.save(cDrink);
            return savedDrink;
        } catch (Exception e) {
            return null;
        }
    }

    // update
    public CDrink updateDrink(long id, CDrink cDrink) {
        if (pDrinkRepository.findById(id).isPresent()) {
            CDrink newDrink = pDrinkRepository.findById(id).get();
            newDrink.setMaNuocUong(cDrink.getMaNuocUong());
            newDrink.setTenNuocUong(cDrink.getTenNuocUong());
            newDrink.setPrice(cDrink.getPrice());
            CDrink savedDrink = pDrinkRepository.save(newDrink);
            return savedDrink;
        } else {
            return null;

        }
    }
}


            
