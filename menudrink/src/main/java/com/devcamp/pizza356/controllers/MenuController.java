package com.devcamp.pizza356.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza356.models.CMenu;
import com.devcamp.pizza356.repository.IMenuRepository;
import com.devcamp.pizza356.services.MenuService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class MenuController {
    @Autowired
    MenuService menuService;

    // get all menu
    @GetMapping("/combo-menu")
    public ResponseEntity<List<CMenu>> getAllComboAPI() {
        try {
            return new ResponseEntity<>(menuService.getAllMenu(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create menu
    @PostMapping("/menu/create")
    // tạo với service
    public ResponseEntity<Object> createUser1(@RequestBody CMenu cMenu) {
        try {
            return new ResponseEntity<>(menuService.createMenu(cMenu), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified menu: " + e.getCause().getCause().getMessage());
        }
    }

    // update with service
    @PutMapping("/menu/update/{id}")
    public ResponseEntity<Object> updateMenuById(@PathVariable("id") Long id, @RequestBody CMenu cMenu) {
        try {
            return new ResponseEntity<>(menuService.updateMenu(id, cMenu), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // delete without service
    @Autowired
    private IMenuRepository pIMenuRepository;

    @DeleteMapping("/menu/delete/{id}")
    public ResponseEntity<Object> deleteMenuById(@PathVariable Long id) {
        try {
            pIMenuRepository.deleteById(id);
            return new ResponseEntity<Object>("Deleted", HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>("Menu does not exist", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
