package com.devcamp.pizza356.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza356.models.CDrink;
import com.devcamp.pizza356.repository.IDrinkRepository;
import com.devcamp.pizza356.services.DrinkService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired
    DrinkService drinkService;

    // get all
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinkAPI() {
        try {
            return new ResponseEntity<>(drinkService.getAllDrinks(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create menu
    @PostMapping("/drink/create")
    // tạo với service
    public ResponseEntity<Object> createUser1(@RequestBody CDrink cDrink) {
        try {
            return new ResponseEntity<>(drinkService.createMenu(cDrink), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified drink: " + e.getCause().getCause().getMessage());
        }
    }

    // update with service
    @PutMapping("/drink/update/{id}")
    public ResponseEntity<Object> updateMenuById(@PathVariable("id") Long id, @RequestBody CDrink cDrink) {
        try {
            return new ResponseEntity<>(drinkService.updateDrink(id, cDrink), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    
    // delete without service
    @Autowired
    private IDrinkRepository pIDrinkRepository;

    @DeleteMapping("/drink/delete/{id}")
    public ResponseEntity<Object> deleteDrinkById(@PathVariable Long id) {
        try {
            pIDrinkRepository.deleteById(id);
            return new ResponseEntity<Object>("Deleted", HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>("Menu does not exist", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
