package com.devcamp.pizza356.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza356.models.CCustomer;
import com.devcamp.pizza356.models.COrder;
import com.devcamp.pizza356.services.CustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    //get all customer list
    @GetMapping("/customers")

    public ResponseEntity<List<CCustomer>> getAllCustomerApi() {
		try {
			return new ResponseEntity<>(customerService.getAllCustomers(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    
            // lấy danh sách order truyen vào customerid
            @GetMapping("/orders")
            public ResponseEntity<Set<COrder>> getOrderByCustomerCode(@RequestParam(value = "customerID") long customerid) {
                try {
                    Set<COrder> orders = customerService.getOrderByCustomerID(customerid);
                    if (orders != null) {
                        return new ResponseEntity<>(orders, HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
                    }
                } catch (Exception e) {
                    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
    
}
