package com.devcamp.pizza356.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza356.models.COrder;

public interface IOrderRepository extends JpaRepository<COrder,Long>{
    
}
