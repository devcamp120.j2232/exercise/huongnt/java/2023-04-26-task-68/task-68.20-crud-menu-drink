package com.devcamp.pizza356.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "menu_combo")

public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "combo", unique = true)
    public char combo;

    @Column(name = "size")
    public String size;

    @Column(name = "grilled")
    public int grilled;

    @Column(name = "salad")
    public String salad;

    @Column(name = "drink")
    public int drink;

    @Column(name = "price")
    public double price;

    public CMenu() {
    }
    public CMenu(char combo, String size, int grilled, String salad, int drink, double price) {
        this.combo = combo;
        this.size = size;
        this.grilled = grilled;
        this.salad = salad;
        this.drink = drink;
        this.price = price;
    }
    public char getCombo() {
        return combo;
    }
    public void setCombo(char combo) {
        this.combo = combo;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }
    public int getGrilled() {
        return grilled;
    }
    public void setGrilled(int grilled) {
        this.grilled = grilled;
    }
    public String getSalad() {
        return salad;
    }
    public void setSalad(String salad) {
        this.salad = salad;
    }
    public int getDrink() {
        return drink;
    }
    public void setDrink(int drink) {
        this.drink = drink;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    

    
    
}
